#include <eiga/codec/Theora.h>
#include <timber/media/IntensityImage.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace timber;
using namespace timber::media;
using namespace eiga::codec;


#if ENABLE_TIMBER_OGG_THEORA==1
void utest_initEncoder()
{
  unique_ptr<ostream> out(new ostringstream());
  Theora::EncodeParameters p;
  p.width =320;
  p.height = 192;
  p.fps = 1000/255.0;
  unique_ptr<Theora> enc = Theora::createEncoder(move(out),p);
}
#endif

