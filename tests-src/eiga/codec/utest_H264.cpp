#include <eiga/codec/H264.h>
#include <timber/media/IntensityImage.h>
#include <iostream>
#include <fstream>
#include <sstream>

using namespace std;
using namespace timber;
using namespace timber::media;
using namespace eiga::codec;

#if ENABLE_TIMBER_H264==1 && ENABLE_TIMBER_MP4==1
void utest_initEncoder()
{
  unique_ptr<ostream> out(new ostringstream());
  H264::EncodeParameters p;
  p.width =320;
  p.height = 200;
  p.fps = 1000/255.0;

  unique_ptr<H264> enc = H264::createEncoder(move(out),p);
}
#endif

