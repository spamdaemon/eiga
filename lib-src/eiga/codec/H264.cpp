#include <eiga/codec/H264.h>
#include <timber/logging.h>
#include <canopy/UnsupportedException.h>

#include <iostream>
#include <sstream>
#include <cstring>

#include <cstdio>
#include <cmath>
#include <math.h>

#if ENABLE_EIGA_H264==1 && ENABLE_EIGA_MP4==1
#include <mp4v2/mp4v2.h>
extern "C" {
#include <x264.h>
}
#endif

using namespace ::std;
using namespace ::canopy;
using namespace ::timber::logging;

namespace eiga {
   namespace codec {
      namespace {

         /**
          * Convert a pointer to a string value in hex.
          * @param p a pointer
          * @return a string in hex
          */
         static ::std::string pointertoHexString(void* ptr)
         {
            ::std::ostringstream out;
            out << ptr;
            return out.str();
         }

         /**
          * Convert a pointer to a string value in hex.
          * @param p a pointer
          * @return a string in hex
          */
         static void* hexStringToPointer(const char* str)
         {
            ::std::istringstream in(str);
            void* ptr = nullptr;
            in >> ptr;
            return ptr;
         }

#if ENABLE_EIGA_H264==1 && ENABLE_EIGA_MP4==1
         static void computeFraction(double value, double absEps, double& num, double& den)
         {
            num = ::round(value);
            den = 1;

            while (true) {
               double delta = (num / den) - value;
               if (delta > absEps) {
                  den += 1;
               }
               else if (delta < -absEps) {
                  num += 1;
               }
               else {
                  return;
               }
            }
         }

         static void x264_log(void*, int i_level, const char* psz, va_list va)
         {
            Level logLevel = Level::INFO;
            switch (i_level) {
               case X264_LOG_ERROR:
                  logLevel = Level::SEVERE;
                  break;
               case X264_LOG_WARNING:
                  logLevel = Level::WARN;
                  break;
               case X264_LOG_INFO:
                  logLevel = Level::INFO;
                  break;
               case X264_LOG_DEBUG:
                  logLevel = Level::DEBUGGING;
                  break;
               default:
                  // don't log
                  return;
            }

            Log logger("timber.media.format.H264");
            if (!logger.isLoggable(logLevel)) {
               return;
            }

            string tmp(256, '\0');
            do {
               int n = ::std::vsnprintf(&tmp[0], tmp.size(), psz, va);
               if (n > 0 && n < (int) tmp.size()) {
                  tmp.resize(n);
                  break;
               }
               if (n > -1) {
                  tmp.resize(n + 1);
               }
               else {
                  tmp.resize(tmp.size() * 2);
               }
            } while (true);
            while (!tmp.empty() && *(tmp.end() - 1) == '\n') {
               tmp.resize(tmp.size() - 1);
            }
            if (!tmp.empty()) {
               logger.log(logLevel, tmp);
            }
         }

         static void* h264_open(const char* name, MP4FileMode mode)
         {
            if (mode != FILEMODE_MODIFY && mode != FILEMODE_CREATE) {
               LogEntry e("timber.media.format.H264");
               e.bug() << "Open called with an unexpected filemode " << mode << doLog;
               return nullptr;
            }

            return hexStringToPointer(name);
         }

         static int h264_seek(void* handle, int64_t pos)
         {
            ostream& out = *reinterpret_cast< ostream*>(handle);
            out.seekp(pos, ios_base::beg);
            return out.good() ? 0 : 1;
         }

         static int h264_read(void*, void*, int64_t, int64_t*, int64_t)
         {
            Log e("timber.media.format.H264");
            e.bug("Read called unexpectedly");
            return 1;
         }

         static int h264_write(void* handle, const void* buffer, int64_t size, int64_t* nout, int64_t /*maxChunkSize*/)
         {
            ostream& out = *reinterpret_cast< ostream*>(handle);

            assert(buffer != 0);
            // write in small chunks to support 32bit archs as well
            *nout = 0;
            while (size > 0) {
               int64_t count = ::std::min((int64_t) (128 * 1024), size);
               out.write((const char*) buffer, (int) count);
               size -= count;
               *nout += count;
               buffer = (const void*) (((const char*) buffer) + count);
            }
            return out.good() ? 0 : 1;
         }

         static int h264_close(void* handle)
         {
            ostream& out = *reinterpret_cast< ostream*>(handle);
            out.flush();
            return 0;
         }

         static int h264_getSize(void* handle, int64_t* nout)
         {
            ostream& out = *reinterpret_cast< ostream*>(handle);
            *nout = 0;
            return 0;
         }

         // this encoder is a pre-1.0 encoder
         struct H264Encoder : public H264
         {
               H264Encoder(unique_ptr< ostream> out, const EncodeParameters& parms) throws (::std::exception)
                     : _frame(0), _bufCapacity(0), _bufSize(0), _buf(0), _parms(parms), _stream(move(out)), _logger(
                           "timber.media.format.H264")
               {
                  // create the MP4 file/stream handle
                  MP4FileProvider fileProvider =
                        { h264_open, h264_seek, h264_read, h264_write, h264_close, h264_getSize };

                  double fps_num, fps_den;
                  computeFraction(_parms.fps, 1.0 / 1024, fps_num, fps_den);

                  LogEntry(_logger).info() << "Recalculated " << _parms.fps << " fps as a fraction of " << fps_num
                        << "/" << fps_den << " (= " << fps_num / fps_den << " )" << doLog;
                  // the maximum number of frames per second is the largest signed integer, i.e. 0x7fff
                  if (fps_num > 0x7fff || fps_den > 0x7fff) {
                     throw ::std::invalid_argument("Invalid fps; cannot express fps as a fraction");
                  }

                  // transform the stream's address into a string and use that as the file name
                  const ::std::string filename(pointertoHexString(_stream.get()));

                  _file = MP4CreateProviderEx(filename.c_str(), 0, &fileProvider, 1, 1, 0, 0, 0, 0);
                  if (_file == MP4_INVALID_FILE_HANDLE) {
                     _logger.bug("Failed to create MP4 file");
                     abort();
                  }
                  // add a video track
                  ::canopy::UInt32 timescale = 90000; // recommended to be set to this value, don't know why (90Khz?)
                  MP4Duration sampleDuration = static_cast< MP4Duration>(::round(timescale * fps_den / fps_num));

                  const uint8_t avcProfileIndication = 66;
                  const uint8_t profile_compat = 224;
                  const uint8_t avcLevelIndication = 30;
                  const uint8_t sampleLenFieldSizeMinusOne = 3;

                  _track = MP4AddH264VideoTrack(_file, timescale, sampleDuration, (uint16_t) parms.width,
                        (uint16_t) parms.height, avcProfileIndication, profile_compat, avcLevelIndication,
                        sampleLenFieldSizeMinusOne);

                  if (MP4_INVALID_TRACK_ID == _track) {
                     // got a bad track
                     _logger.bug("Failed to create MP4 video track");
                     abort();
                  }
                  // use default paramters
                  x264_param_default(&_params);

                  _params.i_threads = 1;
                  _params.b_deterministic = 1;
                  _params.i_width = ::canopy::checked_cast< int>(_parms.width);
                  _params.i_height = ::canopy::checked_cast< int>(_parms.height);
                  _params.i_fps_num = ::canopy::checked_cast< int>(fps_den);
                  _params.i_fps_den = ::canopy::checked_cast< int>(fps_num);

                  _params.pf_log = x264_log;
                  _params.p_log_private = 0;
                  _params.i_log_level = X264_LOG_INFO;
// open_gop force NAL_SEI instead of NAL_SLICE_IDR
                  _params.b_open_gop = 0;

                  _params.i_keyint_max = 30; //* _parms.fps; // generate a key frame every 30 seconds

                  // by not repeating header we avoid playback errors with mplayer
                  _params.b_repeat_headers = 0;
                  _params.b_annexb = 0;

                  LogEntry(_logger).info() << "X264 supported video format : " << _params.vui.i_vidformat << doLog;

                  _encoder = x264_encoder_open(&_params);

                  if (!_encoder) {
                     throw ::std::runtime_error("Could not open X264 encoder");
                  }

                  x264_picture_alloc(&_image, X264_CSP_I420, _params.i_width, _params.i_height);

                  LogEntry(_logger).debugging() << "Picture : " << ::std::endl << "  Format: " << _image.img.i_csp
                        << ::std::endl << "  Planes: " << _image.img.i_plane << ::std::endl << "  Strides: "
                        << _image.img.i_stride[0] << ' ' << _image.img.i_stride[1] << ' ' << _image.img.i_stride[2]
                        << ' ' << _image.img.i_stride[3] << doLog;

                  // encode the header
                  x264_nal_t* nal;
                  int count = 0;
                  x264_encoder_headers(_encoder, &nal, &count);
                  LogEntry(_logger).info() << "Number of header NALs " << count << doLog;
                  muxNals(nal, count);
               }

               ~H264Encoder() throws()
               {
                  if (_encoder) {
                     x264_encoder_close(_encoder);
                  }
                  x264_picture_clean(&_image);
                  MP4Close(_file);
                  _stream.reset(0);
                  delete[] _buf;
               }

               bool muxVideo(int buf_type, const uint8_t* buf, size_t bufSize)
               {
                  const Level level = Level::DEBUGGING;
                  if (buf_type == NAL_SPS) {
                     if (_logger.isLoggable(level)) {
                        LogEntry(_logger).level(level) << "Writing NAL_SPS " << doLog;
                     }

                     MP4AddH264SequenceParameterSet(_file, _track, buf, bufSize);
                  }
                  else if (buf_type == NAL_PPS) {
                     if (_logger.isLoggable(level)) {
                        LogEntry(_logger).level(level) << "Writing NAL PPS " << doLog;
                     }
                     MP4AddH264PictureParameterSet(_file, _track, buf, bufSize);
                  }

                  if (!MP4WriteSample(_file, _track, buf, bufSize, MP4_INVALID_DURATION, 0,
                  true)) {
                     return false;
                  }
                  return true;
               }

               size_t computeNalSize(const x264_nal_t& nal)
               {
                  // this is taken from the sample x264.c file provides by the x264 distribution
                  int sz = nal.i_payload * 2 + 4;
                  assert(sz >= 0);
                  return sz;
               }

               bool encodeNal(x264_nal_t& nal)
               {
                  // drop nals of type 0
                  if (nal.i_type == NAL_UNKNOWN) {
                     _logger.info("Dropping unknown NAL");
                     return false;
                  }

                  // compute the require buffer size
                  int nalSize = computeNalSize(nal);
                  if (nalSize > (int) _bufCapacity) {
                     delete[] _buf;
                     _bufCapacity = (size_t) nalSize;
                     _buf = new uint8_t[_bufCapacity];
                  }
                  memset(_buf, 0, _bufCapacity);

                  // nal_encode will modify the payload value to be the
                  // the encoded size!!
                  int payloadBefore = nal.i_payload;
                  // x264_nal_encode(_encoder, _buf, &nal);

                  const Level level = Level::INFO;

                  if (_logger.isLoggable(level)) {
                     LogEntry(_logger).level(level) << "Nal: payload-size=" << payloadBefore << ", type=" << nal.i_type
                           << ", encoded-size=" << nal.i_payload << doLog;
                  }
                  return true;
               }

               void muxNal(x264_nal_t& nal)
               {
                  // drop nals of type 0
                  if (nal.i_type == NAL_UNKNOWN) {
                     _logger.info("Dropping unknown NAL");
                     return;
                  }

#if 0
                  // do we need to encode a NAL? since we're calling encoder
                  // our we will get encoded NALs. This here serves as documentation
                  // for how to encode a NAL
                  // see documentation for nalu_process in x264_t structure
                  if (!encodeNal(nal)) {
                     return;
                  }
#endif

                  if (!muxVideo(nal.i_type, nal.p_payload, nal.i_payload)) {
                     _logger.info("Failed to mux video nal");
                     throw ::std::runtime_error("Could not write MP4 sample");
                  }
               }

               void muxNals(x264_nal_t* nals, size_t n)
               {
                  for (size_t i = 0; i < n; ++i) {
                     muxNal(nals[i]);
                  }
               }

               void createPicture(const ::timber::media::YUVImage& img) throws (::std::exception)
               {
                  size_t widthY, heightY, strideY;
                  size_t widthU, heightU, strideU;
                  size_t widthV, heightV, strideV;

                  const ::timber::UShort* Y = img.pixels(::timber::media::YUVImage::Y_PLANE, widthY, heightY, strideY);
                  const ::timber::UShort* U = img.pixels(::timber::media::YUVImage::U_PLANE, widthU, heightU, strideU);
                  const ::timber::UShort* V = img.pixels(::timber::media::YUVImage::V_PLANE, widthV, heightV, strideV);

                  if (!Y || !U || !V) {
                     throw ::std::runtime_error("Could not get YUV image data");
                  }

                  if (widthU != widthV || widthY != widthU) {
                     throw ::std::runtime_error("Cannot encode YUV image data; U-V plane does not match");
                  }
                  if (heightU != heightV || heightY != heightU) {
                     throw ::std::runtime_error("Cannot encode YUV image data; U-V plane does not match");
                  }

                  size_t w = ::std::min(widthY, _parms.width);
                  size_t h = ::std::min(heightY, _parms.height);

                  for (size_t i = 0; i < h; ++i) {
                     for (size_t j = 0; j < w; ++j) {
                        size_t kIn = i * widthY + j;
                        size_t kOutY = i * _parms.width + j;
                        size_t kOutUV = (i / 2) * (_parms.width / 2) + (j / 2);

                        _image.img.plane[0][kOutY] = static_cast< unsigned char>(Y[kIn] >> 8);
                        _image.img.plane[1][kOutUV] = static_cast< unsigned char>(U[kIn] >> 8);
                        _image.img.plane[2][kOutUV] = static_cast< unsigned char>(V[kIn] >> 8);
                     }
                  }
                  _image.i_type = X264_TYPE_AUTO;
                  _image.i_qpplus1 = 0;
                  _image.i_pts = _frame;
               }

               void write(const ::timber::media::YUVImage& img) throws (::std::exception)
               {
                  createPicture(img);
                  // create the picture
                  x264_nal_t* nal = 0;
                  int count = 0;
                  x264_picture_t outImage;
                  int status = x264_encoder_encode(_encoder, &nal, &count, &_image, &outImage);
                  if (status >= 0) {
                     ++_frame;
                     muxNals(nal, count);
                  }
                  else if (status < 0) {
                     // some sort of error
                     _logger.warn("Unexpected NAL encoding error");
                  }
               }

               /**
                * Encode an input object.
                * @param input the data to be encoded
                * @param output an output callback that will be invoked for each output produced
                * @return the number of times the output callback was invoked
                */
            public:
               size_t encode(const Input& input, OutputCB& output)
               {
                  return 0;
               }

               size_t width() const throws()
               {
                  return _parms.width;
               }
               size_t height() const throws()
               {
                  return _parms.height;
               }

               /** The current frame */
            private:
               size_t _frame;

               /** The buffer capacity */
            private:
               size_t _bufCapacity;

               /** The current buffer size */
            private:
               int _bufSize;

               /** An output buffer */
            private:
               uint8_t* _buf;

               /** The encoder parameters */
            private:
               const EncodeParameters _parms;

               /** The target stream */
            private:
               unique_ptr< ostream> _stream;

               /** The h264 logger */
            private:
               Log _logger;

               /** The encoder types */
            private:
               x264_param_t _params;
               x264_t* _encoder;
               x264_picture_t _image;

               /** The mp4 type */
            private:
               MP4FileHandle _file;
               MP4TrackId _track;

         };
#endif
      }

      H264::EncodeParameters::EncodeParameters() throws()
            : width(0), height(0), fps(0)
      {
      }

      H264::H264() throws()
      {
      }
      H264::~H264() throws()
      {
      }

      ::std::unique_ptr< H264> H264::createEncoder(::std::unique_ptr< ::std::ostream> out,
            const EncodeParameters& parms) throws (::std::exception)
      {
#if ENABLE_EIGA_H264==1 && ENABLE_EIGA_MP4==1
         return unique_ptr < H264 > (new H264Encoder(move(out), parms));
#else
         throw ::canopy::UnsupportedException("H264 encoding not supported");
#endif
      }

      bool H264::isSupported() throw()
      {
         return ENABLE_EIGA_H264 != 0 && ENABLE_EIGA_MP4 != 0;
      }

   }
}

