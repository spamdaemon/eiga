#ifndef _EIGA_CODEC_THEORA_H
#define _EIGA_CODEC_THEORA_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_YUVIMAGE_H
#include <timber/media/YUVImage.h>
#endif


#include <iosfwd>

namespace eiga {
   namespace codec {

      class Theora
      {
         private:
            Theora(const Theora&);
            Theora&operator=(const Theora&);

            /** Default constructor */
         public:
            class EncodeParameters
            {

                  /** Default constructor */
               public:
                  EncodeParameters() throws();

               public:
                  size_t width, height;
                  double fps;
            };

            /** Default constructor */
         protected:
            Theora() throws();

            /** Destructor */
         public:
            virtual ~Theora() throws() = 0;

            /**
             * Determine if Theora is supported.
             * @return true if Theora is supported
             */
         public:
            static bool isSupported() throw();

            /**
             * Create a new Theora encoder.
             * @param stream an output stream
             * @param parms the encoder parameters
             * @throws ::std::exception if the enc
             */
         public:
            static ::std::unique_ptr< Theora> createEncoder(::std::unique_ptr< ::std::ostream> stream,
                  const EncodeParameters& parms) throws (::std::exception);

            /**
             * Get the width of this video in pixels.
             * @return the width of this image
             */
         public:
            virtual size_t width() const throws() = 0;

            /**
             * Get the height of this video in pixels
             * @return the height of this image
             */
         public:
            virtual size_t height() const throws() = 0;

            /**
             * Encode another image with theora. This only works on instances create with createEncoder().
             * If the image does not match the width and height of this encoder, then the image is cropped
             * to that size.
             * @param img an image
             * @throws ::std::exception if the image could not be encoded
             */
         public:
            virtual void write(const ::timber::media::YUVImage& img) throws (::std::exception) = 0;

      };
   }
}

#endif
