#ifndef _EIGA_CODEC_H264_H
#define _EIGA_CODEC_H264_H

#ifndef _TIMBER_H
#include <timber/timber.h>
#endif

#ifndef _TIMBER_MEDIA_YUVIMAGE_H
#include <timber/media/YUVImage.h>
#endif

#ifndef _EIGA_ENCODER_H
#include <eiga/Encoder.h>
#endif

#include <iosfwd>

namespace eiga {
   namespace codec {

      class H264 : public Encoder< MediaType::VIDEO>
      {
         private:
            H264(const H264&);
            H264&operator=(const H264&);

            /** The encoder parameters */
         public:
            class EncodeParameters
            {

                  /** Default constructor */
               public:
                  EncodeParameters() throws();

               public:
                  size_t width, height;
                  double fps;
            };

            /** Default constructor */
         protected:
            H264() throws();

            /** Destructor */
         public:
            virtual ~H264() throws() = 0;

            /**
             * Determine if H264 is supported.
             * @return true if H264 is supported
             */
         public:
            static bool isSupported() throw();

            /**
             * Create a new H264 encoder. The encoder requires a fixed frame rate at this time.
             * The number of frames per second actually used by the encoder will vary slightly
             * due to round-off.
             * @param stream an output stream
             * @param parms the encoder parameters
             * @throws ::std::exception if the enc
             * @throws ::std::invalid_argument if fps > 0x7fff or fps <= 1.0/0x7fff
             */
         public:
            static ::std::unique_ptr< H264> createEncoder(::std::unique_ptr< ::std::ostream> stream,
                  const EncodeParameters& parms) throws (::std::exception);

            /**
             * Get the width of this video in pixels.
             * @return the width of this image
             */
         public:
            virtual size_t width() const throws() = 0;

            /**
             * Get the height of this video in pixels
             * @return the height of this image
             */
         public:
            virtual size_t height() const throws() = 0;

            /**
             * Encode another image with h264. This only works on instances created with createEncoder().
             * If the image does not match the width and height of this encoder, then the image is cropped
             * to that size.
             * @param img an image
             * @throws ::std::exception if the image could not be encoded
             */
         public:
            virtual void write(const ::timber::media::YUVImage& img) throws (::std::exception) = 0;
      };
   }
}

#endif
