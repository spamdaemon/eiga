#include <eiga/codec/Theora.h>
#include <timber/logging.h>
#include <canopy/UnsupportedException.h>

#include <iostream>
#include <cstring>
#include <cmath>

#if ENABLE_EIGA_OGG_THEORA==1
#include <theora/theora.h>
#include <theora/theoraenc.h>
#include <theora/theoradec.h>
#include <ogg/ogg.h>
#endif

using namespace ::std;
using namespace ::canopy;
using namespace ::timber::logging;

namespace eiga {
   namespace codec {
      namespace {
#if ENABLE_EIGA_OGG_THEORA==1
      // the OC_PF_RSVD is not a supported format and is used to indicate the end of the array
      static theora_pixelformat SUPPORTED_PIXELFORMATS[] = {OC_PF_444, OC_PF_420, OC_PF_422, OC_PF_RSVD};

      // the OC_PF_RSVD is not a supported format and is used to indicate the end of the array
      static th_pixel_fmt TH_SUPPORTED_PIXELFORMATS[] = {TH_PF_444, TH_PF_420, TH_PF_422, TH_PF_RSVD};

      static void computeFraction(double value, double absEps, double& num, double& den)
      {
         num = ::round(value);
         den = 1;

         while (true) {
            double delta = (num / den) - value;
            if (delta > absEps) {
               den += 1;
            }
            else if (delta < -absEps) {
               num += 1;
            }
            else {
               return;
            }
         }
      }

      // this encoder is a pre-1.0 encoder
      struct Theora0Encoder : public Theora
      {
         Theora0Encoder(unique_ptr< ostream> out, const EncodeParameters& parms) throws (::std::exception) :
         _parms(parms), _stream(move(out)), _logger("timber.media.format.Theora")
         {
            LogEntry(_logger).info() << theora_version_string() << doLog;

            int status;
            if ((status = ogg_stream_init(&_oggStream, 1))) {
               LogEntry(_logger).severe() << "Could not initialize ogg stream ( " << status << ")" << doLog;
               throw ::std::runtime_error("Could not initialize ogg stream");
            }

            double fps_num, fps_den;
            computeFraction(parms.fps, 1.0 / 1024, fps_num, fps_den);

            theora_info_init (_info);

            _info[0].width = (int) parms.width;
            _info[0].height = (int) parms.height;
            _info[0].frame_width = _info[0].width;
            _info[0].frame_height = _info[0].height;
            _info[0].fps_numerator = ::canopy::checked_cast< UInt32>(fps_num);
            _info[0].fps_denominator = ::canopy::checked_cast< UInt32>(fps_den);
            _info[0].aspect_numerator = 1; //_info[0].width;
            _info[0].aspect_denominator = 1;//_info[0].height;
            _info[0].colorspace = OC_CS_UNSPECIFIED;
            _info[0].target_bitrate = 0;
            _info[0].quality = 1;// 0 (lowest) to 63 (highest)
            _info[0].quick_p = 1;
            // do auto-keyframing, if possible
            _info[0].keyframe_auto_p = 1;
            // force a keyframe every 10 seconds
            _info[0].keyframe_frequency_force = 10 * ::std::max(_info[0].fps_numerator
                  / _info[0].fps_numerator, _info[0].fps_numerator / _info[0].fps_denominator);

            // find a pixel format that we can support
            {
               for (size_t i = 0; SUPPORTED_PIXELFORMATS[i] != OC_PF_RSVD; ++i) {
                  _info[0].pixelformat = SUPPORTED_PIXELFORMATS[i];
                  status = theora_encode_init(_state, _info);
                  if (!status) {
                     break;
                  }
               }

               if (status) {
                  LogEntry(_logger).severe() << "Could not initialize Theora encoder ( " << status << ")"
                  << doLog;
                  ogg_stream_clear(&_oggStream);
                  theora_info_clear(_info);
                  throw ::std::runtime_error("Could not initialize Theora encoder");
               }
            }

            // encode the theora headers (they are NOT optional!)
            {
               // first, do the initialization header
               {
                  if ((status = theora_encode_header(_state, &_oggPacket))) {
                     LogEntry(_logger).severe() << "Could not write Theora header ( " << status << ")"
                     << doLog;
                     ogg_stream_clear(&_oggStream);
                     theora_info_clear(_info);
                     throw ::std::runtime_error("Could not write Theora header");
                  }
                  writeOggPacket(true);
               }

               // second header is the comment header, which we'll leave empty
               {
                  theora_comment comment[1];
                  theora_comment_init(comment);
                  if ((status = theora_encode_comment(comment, &_oggPacket))) {
                     LogEntry(_logger).severe() << "Could not write Theora comment header ( " << status << ")"
                     << doLog;
                     ogg_stream_clear(&_oggStream);
                     theora_info_clear(_info);
                     throw ::std::runtime_error("Could not write Theora comment");
                  }
                  writeOggPacket(true);
               }

               // last, write the tables
               {
                  if ((status = theora_encode_tables(_state, &_oggPacket))) {
                     LogEntry(_logger).severe() << "Could not write Theora comment header ( " << status << ")"
                     << doLog;
                     ogg_stream_clear(&_oggStream);
                     theora_info_clear(_info);
                     throw ::std::runtime_error("Could not write Theora header");
                  }
                  writeOggPacket(true);
               }
            }

            // check that we support the pixel format
            _yuv.y_width = _info[0].width;
            _yuv.y_height = _info[0].height;
            _yuv.y_stride = _yuv.y_width;

            switch (_info[0].pixelformat) {
               case OC_PF_420:
               _yuv.uv_width = _yuv.y_width / 2;
               _yuv.uv_height = _yuv.y_height / 2;
               _yuv.uv_stride = _yuv.uv_width;
               break;
               case OC_PF_444:
               _yuv.uv_width = _yuv.y_width;
               _yuv.uv_height = _yuv.y_height;
               _yuv.uv_stride = _yuv.uv_width;
               break;
               case OC_PF_422:
               _yuv.uv_width = _yuv.y_width / 2;
               _yuv.uv_height = _yuv.y_height;
               _yuv.uv_stride = _yuv.uv_width;
               break;
               default:
               LogEntry(_logger).severe() << "Unsupported pixel format " << _info[0].pixelformat << doLog;
               ogg_stream_clear(&_oggStream);
               theora_info_clear(_info);
               throw ::std::runtime_error("Unsupported pixel format");
            }

            _yuv.y = new unsigned char[_yuv.y_width * _yuv.y_height + 2 * _yuv.uv_width * _yuv.uv_height];
            _yuv.u = _yuv.y + _yuv.y_width * _yuv.y_height;
            _yuv.v = _yuv.u + _yuv.uv_width * _yuv.uv_height;
         }

         ~Theora0Encoder() throws()
         {
            try {
               flushOgg(true);
            }
            catch (const ::std::exception& e) {
               // ignore this; nothing we can do about it
               _logger.caught("Exception while flushing theora stream", e);
            }

            delete[] _yuv.y;
            _yuv.y = _yuv.u = _yuv.v = 0;

            //	    theora_clear(_state);
            theora_info_clear(_info);

            ogg_stream_clear(&_oggStream);
            ogg_packet_clear(&_oggPacket);
         }

         void writeOggPacket(bool clear)
         {
            int status = ogg_stream_packetin(&_oggStream, &_oggPacket);
            if (status) {
               LogEntry(_logger).severe() << "Internal ogg error ( " << status << ")" << doLog;
               if (clear) {
                  ogg_stream_clear(&_oggStream);
                  theora_info_clear(_info);
               }

               throw ::std::runtime_error("Internal OGG error");
            }
         }

         void flushOgg(bool lastPacket)
         {
            int status;
            // get any data that's ready and pass it on to ogg
            if ((status = theora_encode_packetout(_state, lastPacket, &_oggPacket)) == 0) {
               // nothing available yet
               return;
            }
            if (status != 1) {
               // ok, no more data available
            }
            writeOggPacket(false);

            while ((status = ogg_stream_pageout(&_oggStream, &_oggPage))) {
               _stream->write(reinterpret_cast< const char*> (_oggPage.header), _oggPage.header_len);
               _stream->write(reinterpret_cast< const char*> (_oggPage.body), _oggPage.body_len);
            }

            if (lastPacket) {
               status = ogg_stream_flush(&_oggStream, &_oggPage);
               if (status) {
                  _oggPage.header[5] |= 0x04;
               }
               if (status) {
                  _stream->write(reinterpret_cast< const char*> (_oggPage.header), _oggPage.header_len);
                  _stream->write(reinterpret_cast< const char*> (_oggPage.body), _oggPage.body_len);
               }
            }
         }

         void write(const ::timber::media::YUVImage& img) throws (::std::exception)
         {

            size_t widthY, heightY, strideY;
            size_t widthU, heightU, strideU;
            size_t widthV, heightV, strideV;

            const ::timber::UShort* Y = img.pixels(::timber::media::YUVImage::Y_PLANE, widthY, heightY, strideY);
            const ::timber::UShort* U = img.pixels(::timber::media::YUVImage::U_PLANE, widthU, heightU, strideU);
            const ::timber::UShort* V = img.pixels(::timber::media::YUVImage::V_PLANE, widthV, heightV, strideV);

            if (!Y || !U || !V) {
               throw ::std::runtime_error("Could not get YUV image data");
            }

            if (widthU != widthV || widthY != widthU) {
               throw ::std::runtime_error("Cannot encode YUV image data; U-V plane does not match");
            }
            if (heightU != heightV || heightY != heightU) {
               throw ::std::runtime_error("Cannot encode YUV image data; U-V plane does not match");
            }

            ::std::memset(_yuv.y, 0, _yuv.y_width * _yuv.y_height + 2 * _yuv.uv_width * _yuv.uv_height);

            size_t w = ::std::min(widthY, (size_t) _yuv.y_width);
            size_t h = ::std::min(heightY, (size_t) _yuv.y_height);

            switch (_info[0].pixelformat) {
               case OC_PF_420: {
                  for (size_t i = 0; i < h; ++i) {
                     for (size_t j = 0; j < w; ++j) {
                        size_t kIn = i * widthY + j;
                        size_t kOutY = i * _yuv.y_width + j;
                        size_t kOutUV = (i / 2) * _yuv.uv_width + (j / 2);

                        _yuv.y[kOutY] = static_cast< unsigned char> (Y[kIn] >> 8);
                        _yuv.u[kOutUV] = static_cast< unsigned char> (U[kIn] >> 8);
                        _yuv.v[kOutUV] = static_cast< unsigned char> (V[kIn] >> 8);
                     }
                  }
                  break;
               }
               case OC_PF_422: {
                  for (size_t i = 0; i < h; ++i) {
                     for (size_t j = 0; j < w; ++j) {
                        size_t kIn = i * widthY + j;
                        size_t kOutY = i * _yuv.y_width + j;
                        size_t kOutUV = (i) * _yuv.uv_width + (j / 2);

                        _yuv.y[kOutY] = static_cast< unsigned char> (Y[kIn] >> 8);
                        _yuv.u[kOutUV] = static_cast< unsigned char> (U[kIn] >> 8);
                        _yuv.v[kOutUV] = static_cast< unsigned char> (V[kIn] >> 8);
                     }
                  }
                  break;
               }
               case OC_PF_444: {
                  for (size_t i = 0; i < h; ++i) {
                     for (size_t j = 0; j < w; ++j) {
                        size_t kIn = i * widthY + j;
                        size_t kOut = i * _yuv.y_width + j;

                        _yuv.y[kOut] = static_cast< unsigned char> (Y[kIn] >> 8);
                        _yuv.u[kOut] = static_cast< unsigned char> (U[kIn] >> 8);
                        _yuv.v[kOut] = static_cast< unsigned char> (V[kIn] >> 8);
                     }
                  }
                  break;
               }
               default:
               throw ::std::runtime_error("Unknown pixelformat");
            };

            // flush ogg, but don't force it
            flushOgg(false);

            // encode with theora
            {
               int status = theora_encode_YUVin(_state, &_yuv);
               if (status != 0) {
                  if (status == OC_EINVAL) {
                     throw ::std::runtime_error("Encoder not ready");
                  }
                  throw ::std::runtime_error("Invalid image; differs from first image");
               }
            }

         }
         size_t width() const throws()
         {
            return _parms.width;
         }
         size_t height() const throws()
         {
            return _parms.height;
         }

         /** The encoder parameters */
         private:
         const EncodeParameters _parms;

         /** The encoder */
         private:
         theora_info _info[1];
         theora_state _state[1];
         yuv_buffer _yuv;
         char* _yuvData;

         /** The ogg support */
         private:
         ogg_stream_state _oggStream;
         ogg_packet _oggPacket;
         ogg_page _oggPage;

         /** The target stream */
         private:
         unique_ptr< ostream> _stream;

         /** The theora logger */
         private:
         Log _logger;
      };

      struct Theora1Encoder : public Theora
      {
         Theora1Encoder(unique_ptr< ostream> out, const EncodeParameters& parms) throws (::std::exception) :
         _parms(parms), _state(0), _stream(move(out)), _logger("timber.media.format.Theora")
         {
            LogEntry(_logger).info() << th_version_string() << doLog;

            int status = 0;
            if ((status = ogg_stream_init(&_oggStream, 1))) {
               LogEntry(_logger).severe() << "Could not initialize ogg stream ( " << status << ")" << doLog;
               throw ::std::runtime_error("Could not initialize ogg stream");
            }

            double fps_num, fps_den;
            computeFraction(parms.fps, 1.0 / 1024, fps_num, fps_den);

            int xwidth = (int) parms.width;
            int xheight = (int) parms.height;
            {
               th_info info[1];
               th_info_init(info);
               info[0].pic_x = 0;
               info[0].pic_y = 0;

               info[0].pic_width = xwidth;
               info[0].pic_height = xheight;
               info[0].frame_width = info[0].pic_width;
               info[0].frame_height = info[0].pic_height;
               info[0].fps_numerator = ::canopy::checked_cast< UInt32>(fps_num);
               info[0].fps_denominator = ::canopy::checked_cast< UInt32>(fps_den);
               info[0].aspect_numerator = 1; //_info[0].width;
               info[0].aspect_denominator = 1;//_info[0].height;
               info[0].colorspace = TH_CS_UNSPECIFIED;
               info[0].target_bitrate = 0;
               info[0].quality = 32;// 0 (lowest) to 63 (highest)

               // find a pixel format that we can support
               for (size_t i = 0; TH_SUPPORTED_PIXELFORMATS[i] != TH_PF_NFORMATS; ++i) {
                  info[0].pixel_fmt = TH_SUPPORTED_PIXELFORMATS[i];
                  _state = th_encode_alloc(info);
                  if (_state) {
                     break;
                  }
               }
               _pixelFormat = info[0].pixel_fmt;
               th_info_clear(info);

               if (!_state) {
                  LogEntry(_logger).severe() << "Could not initialize Theora encoder" << doLog;
                  ogg_stream_clear(&_oggStream);
                  throw ::std::runtime_error("Could not initialize Theora encoder");
               }
            }

            // encode the theora headers (they are NOT optional!)
            {
               // second header is the comment header, which we'll leave empty
               th_comment comment[1];
               th_comment_init(comment);
               do {
                  status = th_encode_flushheader(_state, comment, &_oggPacket);
                  if (status == TH_EFAULT) {
                     LogEntry(_logger).severe() << "Could not write Theora header ( " << status << ")"
                     << doLog;
                     ogg_stream_clear(&_oggStream);
                     th_encode_free(_state);
                     _state = 0;
                     throw ::std::runtime_error("Could not write Theora header");
                  }

                  if (status == 0) {
                     break;
                  }
                  writeOggPacket(true);
               }while (true);
               writeOggPage(false);
               th_comment_clear(comment);
            }

            // check that we support the pixel format
            _ycbcr[0].width = xwidth;
            _ycbcr[0].height = xheight;
            _ycbcr[0].stride = _ycbcr[0].width;
            _ycbcr[0].data = 0;

            switch (_pixelFormat) {
               case TH_PF_420:
               _ycbcr[1].width = _ycbcr[2].width = _ycbcr[0].width / 2;
               _ycbcr[1].height = _ycbcr[2].height = _ycbcr[0].height / 2;
               _ycbcr[1].stride = _ycbcr[2].stride = _ycbcr[1].width;
               break;
               case TH_PF_444:
               _ycbcr[1] = _ycbcr[2] = _ycbcr[0];
               break;
               case TH_PF_422:
               _ycbcr[1].width = _ycbcr[2].width = _ycbcr[0].width / 2;
               _ycbcr[1].height = _ycbcr[2].height = _ycbcr[0].height;
               _ycbcr[1].stride = _ycbcr[2].stride = _ycbcr[1].width;
               break;
               default:
               LogEntry(_logger).severe() << "Unsupported pixel format " << _pixelFormat << doLog;
               ogg_stream_clear(&_oggStream);
               th_encode_free(_state);
               _state = 0;
               throw ::std::runtime_error("Unsupported pixel format");
            }

            try {
               size_t sz[] = {
                  _ycbcr[0].width * _ycbcr[0].height, _ycbcr[1].width * _ycbcr[1].height, _ycbcr[2].width
                  * _ycbcr[2].height};

               _ycbcr[0].data = new unsigned char[sz[0] + sz[1] + sz[2]];
               _ycbcr[1].data = _ycbcr[0].data + sz[0];
               _ycbcr[2].data = _ycbcr[1].data + sz[1];
            }
            catch (...) {
               th_encode_free(_state);
               _state = 0;
               throw;
            }
         }

         ~Theora1Encoder() throws()
         {
            try {
               flushOgg(true);
            }
            catch (const ::std::exception& e) {
               // ignore this; nothing we can do about it
               _logger.caught("Exception while flushing theora stream", e);
            }

            delete[] _ycbcr[0].data;
            _ycbcr[0].data = _ycbcr[1].data = _ycbcr[2].data = 0;

            if (_state) {
               th_encode_free(_state);
               _state = 0;
            }

            ogg_stream_clear(&_oggStream);
         }

         void writeOggPacket(bool clear)
         {
            int status = ogg_stream_packetin(&_oggStream, &_oggPacket);
            if (status) {
               LogEntry(_logger).severe() << "Internal ogg error ( " << status << ")" << doLog;
               if (clear) {
                  ogg_stream_clear(&_oggStream);
                  if (_state) {
                     th_encode_free(_state);
                     _state = 0;
                  }
               }

               throw ::std::runtime_error("Internal OGG error");
            }
         }

         void writeOggPage(bool lastPacket)
         {
            for (int status = 0; (status = ogg_stream_pageout(&_oggStream, &_oggPage));) {
               _stream->write(reinterpret_cast< const char*> (_oggPage.header), _oggPage.header_len);
               _stream->write(reinterpret_cast< const char*> (_oggPage.body), _oggPage.body_len);
            }

            if (lastPacket) {
               int status = ogg_stream_flush(&_oggStream, &_oggPage);
               if (status) {
                  _oggPage.header[5] |= 0x04;
               }
               if (status) {
                  _stream->write(reinterpret_cast< const char*> (_oggPage.header), _oggPage.header_len);
                  _stream->write(reinterpret_cast< const char*> (_oggPage.body), _oggPage.body_len);
               }
            }
            (*_stream) << ::std::flush;
         }

         void flushOgg(bool lastPacket)
         {
            while (true) {
               // get any data that's ready and pass it on to ogg
               int status = th_encode_packetout(_state, lastPacket, &_oggPacket);
               if (status == TH_EFAULT) {
                  throw ::std::runtime_error("th_encode_packetout failed");
               }
               if (status == 0) {
                  // nothing available yet
                  break;
               }
               writeOggPacket(false);
            }

            writeOggPage(lastPacket);
         }

         void write(const ::timber::media::YUVImage& img) throws (::std::exception)
         {
            size_t widthY, heightY, strideY;
            size_t widthU, heightU, strideU;
            size_t widthV, heightV, strideV;

            const ::timber::UShort* Y = img.pixels(::timber::media::YUVImage::Y_PLANE, widthY, heightY, strideY);
            const ::timber::UShort* U = img.pixels(::timber::media::YUVImage::U_PLANE, widthU, heightU, strideU);
            const ::timber::UShort* V = img.pixels(::timber::media::YUVImage::V_PLANE, widthV, heightV, strideV);

            if (!Y || !U || !V) {
               throw ::std::runtime_error("Could not get YUV image data");
            }

            if (widthU != widthV || widthY != widthU) {
               throw ::std::runtime_error("Cannot encode YUV image data; U-V plane does not match");
            }
            if (heightU != heightV || heightY != heightU) {
               throw ::std::runtime_error("Cannot encode YUV image data; U-V plane does not match");
            }

            ::std::memset(_ycbcr[0].data, 0, (_ycbcr[0].width * _ycbcr[0].height));
            ::std::memset(_ycbcr[1].data, 0, (_ycbcr[1].width * _ycbcr[1].height));
            ::std::memset(_ycbcr[2].data, 0, (_ycbcr[2].width * _ycbcr[2].height));

            size_t w = ::std::min(widthY, (size_t) _ycbcr[0].width);
            size_t h = ::std::min(heightY, (size_t) _ycbcr[0].height);

            switch (_pixelFormat) {
               case TH_PF_420: {
                  for (size_t i = 0; i < h; ++i) {
                     for (size_t j = 0; j < w; ++j) {
                        size_t kIn = i * widthY + j;
                        size_t kOutY = i * _ycbcr[0].width + j;
                        size_t kOutUV = (i / 2) * _ycbcr[1].width + (j / 2);

                        _ycbcr[0].data[kOutY] = static_cast< unsigned char> (Y[kIn] >> 8);
                        _ycbcr[1].data[kOutUV] = static_cast< unsigned char> (U[kIn] >> 8);
                        _ycbcr[2].data[kOutUV] = static_cast< unsigned char> (V[kIn] >> 8);
                     }
                  }
                  break;
               }
               case TH_PF_422: {
                  for (size_t i = 0; i < h; ++i) {
                     for (size_t j = 0; j < w; ++j) {
                        size_t kIn = i * widthY + j;
                        size_t kOutY = i * _ycbcr[0].width + j;
                        size_t kOutUV = (i) * _ycbcr[1].width + (j / 2);

                        _ycbcr[0].data[kOutY] = static_cast< unsigned char> (Y[kIn] >> 8);
                        _ycbcr[1].data[kOutUV] = static_cast< unsigned char> (U[kIn] >> 8);
                        _ycbcr[2].data[kOutUV] = static_cast< unsigned char> (V[kIn] >> 8);
                     }
                  }
                  break;
               }
               case TH_PF_444: {
                  for (size_t i = 0; i < h; ++i) {
                     for (size_t j = 0; j < w; ++j) {
                        size_t kIn = i * widthY + j;
                        size_t kOut = i * _ycbcr[0].width + j;

                        _ycbcr[0].data[kOut] = static_cast< unsigned char> (Y[kIn] >> 8);
                        _ycbcr[1].data[kOut] = static_cast< unsigned char> (U[kIn] >> 8);
                        _ycbcr[2].data[kOut] = static_cast< unsigned char> (V[kIn] >> 8);
                     }
                  }
                  break;
               }
               default:
               throw ::std::runtime_error("Unknown pixelformat");
            };

            // flush ogg, but don't force it
            flushOgg(false);

            // encode with theora
            {
               int status = th_encode_ycbcr_in(_state, _ycbcr);
               if (status != 0) {
                  if (status == TH_EINVAL) {
                     throw ::std::runtime_error("Encoder not ready");
                  }

                  throw ::std::runtime_error("Invalid image; differs from first image");
               }
            }

         }
         size_t width() const throws()
         {
            return _parms.width;
         }
         size_t height() const throws()
         {
            return _parms.height;
         }

         /** The encoder parameters */
         private:
         const EncodeParameters _parms;

         /** The encoder */
         private:
         th_pixel_fmt _pixelFormat;
         th_enc_ctx* _state;
         th_ycbcr_buffer _ycbcr;
         char* _yuvData;

         /** The ogg support */
         private:
         ogg_stream_state _oggStream;
         ogg_packet _oggPacket;
         ogg_page _oggPage;

         /** The target stream */
         private:
         unique_ptr< ostream> _stream;

         /** The theora logger */
         private:
         Log _logger;
      };
#endif
   }

   Theora::EncodeParameters::EncodeParameters() throws()
         : width(0), height(0), fps(0)
   {
   }

   Theora::Theora() throws()
   {
   }
   Theora::~Theora() throws()
   {
   }

   unique_ptr< Theora> Theora::createEncoder(::std::unique_ptr< ::std::ostream> out,
         const EncodeParameters& parms) throws (::std::exception)
   {
#if ENABLE_EIGA_OGG_THEORA==1
      return unique_ptr< Theora> (new Theora1Encoder(move(out), parms));
#else
      throw ::canopy::UnsupportedException("Theora encoding not supported");
#endif
   }

   bool Theora::isSupported() throw()
   {
      return ENABLE_EIGA_OGG_THEORA != 0;
   }

}
}
