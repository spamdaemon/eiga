#ifndef _EIGA_ENCODER_H
#define _EIGA_ENCODER_H

#ifndef _EIGA_H
#include <eiga/eiga.h>
#endif

#ifndef _EIGA_MEDIATYPE_H
#include <eiga/MediaType.h>
#endif

#ifndef _EIGA_ENCODED_H
#include <eiga/Encoded.h>
#endif

#include <functional>

namespace eiga {

   /**
    * An encoder for input data.
    */
   template<MediaType MT>
   class Encoder
   {
         Encoder(const Encoder&) = delete;
         Encoder& operator=(const Encoder&) = delete;

         /** The input type */
      public:
         typedef typename CodecHelper< MT>::Decoded Input;

         /** The output type */
      public:
         typedef typename CodecHelper< MT>::Encoded Output;

         /** The output function */
      public:
         typedef ::std::function< void(const Output&)> OutputCB;

         /** Constructor */
      protected:
         Encoder() throws()
         {
         }

         /** Destructor */
      public:
         virtual ~Encoder() throws();

         /**
          * Encode an input object.
          * @param input the data to be encoded
          * @param output an output callback that will be invoked for each output produced
          * @return the number of times the output callback was invoked
          */
      public:
         virtual size_t encode(const Input& input, OutputCB& output) = 0;
   };

   template<MediaType MT> Encoder< MT>::~Encoder() throws()
   {
   }


}

#endif
