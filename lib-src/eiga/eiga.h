#ifndef _EIGA_H
#define _EIGA_H

#ifndef _CANOPY_H
#include <canopy/canopy.h>
#endif

/**
 * The eiga namespace defines classes for working
 * with different types of media. It is an implementation
 * of the basic functions provided by the ::timber::media package.
 */
namespace eiga {
}

#endif
