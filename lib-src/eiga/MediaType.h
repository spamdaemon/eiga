#ifndef _EIGA_MEDIATYPE_H
#define _EIGA_MEDIATYPE_H

namespace eiga {

   /**
    * The support mediatypes.
    */
   enum class MediaType
   {
      /** An still-image */
      IMAGE,
      /** An audio stream */
      AUDIO,
      /** An image stream */
      VIDEO,
      /** Text */
      TEXT
   };

}

#endif
