#ifndef _EIGA_CODEC_H
#define _EIGA_CODEC_H

#ifndef _EIGA_H
#include <eiga/eiga.h>
#endif

#ifndef _EIGA_MEDIATYPE_H
#include <eiga/MediaType.h>
#endif

#ifndef _EIGA_ENCODER_H
#include <eiga/Encoder.h>
#endif

#ifndef _EIGA_DECODER_H
#include <eiga/Decoder.h>
#endif

#include <memory>

namespace eiga {

   /**
    * A codec is algorithm used for encoding and decoding different types of media data.
    * A codec works on a single stream of media (e.g. video) and may be combined with other types
    * (e.g. audio) using a ::eiga::Container.
    */
   template<MediaType MT>
   class Codec
   {
         Codec(const Codec&) = delete;
         Codec& operator=(const Codec&) = delete;

         /** The default constructor */
      protected:
         Codec() throws()
         {
         }

         /** Destructor */
      public:
         virtual ~Codec() throws() = 0;

         /**
          * Create an encoder.
          * @return an encoder or null if an encoder could not be created
          */
      public:
         ::std::unique_ptr< Encoder< MT> > createEncoder()throws() =0;

         /**
          * Create a decoder
          * @return a decoder or null if the  decoder could not be created
          */
      public:
         ::std::unique_ptr< Decoder< MT> > createDecoder()throws() = 0;
   };

   template<MediaType MT> Codec< MT>::~Codec() throws()
   {
   }

   /**
    * A text codec
    */
   typedef Codec< MediaType::TEXT> TextCodec;

   /**
    * An image codec
    */
   typedef Codec< MediaType::IMAGE> ImageCodec;

   /**
    * A video codec
    */
   typedef Codec< MediaType::VIDEO> VideoCodec;

   /**
    * An audio codec
    */
   typedef Codec< MediaType::AUDIO> AudioCodec;

}

#endif
