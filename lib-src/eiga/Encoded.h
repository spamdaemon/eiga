#ifndef EIGA_ENCODED_H
#define EIGA_ENCODED_H

#ifndef _EIGA_H
#include <eiga/eiga.h>
#endif

#ifndef _EIGA_MEDIATYPE_H
#include <eiga/MediaType.h>
#endif

#ifndef _TIMBER_MEDIA_AUDIO_H
#include <timber/media/AudioSample.h>
#endif

#ifndef _TIMBER_MEDIA_VIDEO_H
#include <timber/media/VideoSample.h>
#endif

#ifndef _TIMBER_MEDIA_IMAGE_H
#include <timber/media/Image.h>
#endif

namespace eiga {
   /**
    * An encoded object is the input to a decoder and the output of an encoder.
    * Often, an encoded object is just a byte wrapper.
    */
   template<MediaType MT>
   class Encoded
   {
         Encoded(const Encoded&) = delete;
         Encoded& operator=(const Encoded&) = delete;

         /** Default constructor */
      protected:
         inline Encoded()
         {
         }

         /** Destructor */
      public:
         virtual ~Encoded() = 0;
   };

   /**
    * A simple helper class
    */
   template<MediaType MT>
   class CodecHelper
   {
   };

   template<>
   struct CodecHelper< MediaType::TEXT>
   {
         typedef ::std::string& Decoded;
         typedef ::eiga::Encoded< MediaType::TEXT> Encoded;
   };

   template<>
   struct CodecHelper< MediaType::AUDIO>
   {
         typedef ::timber::media::AudioSample Decoded;
         typedef ::eiga::Encoded< MediaType::AUDIO> Encoded;
   };

   template<>
   struct CodecHelper< MediaType::VIDEO>
   {
         typedef ::timber::media::VideoSample Decoded;
         typedef ::eiga::Encoded< MediaType::VIDEO> Encoded;
   };

   template<>
   struct CodecHelper< MediaType::IMAGE>
   {
         typedef ::timber::media::Image Decoded;
         typedef ::eiga::Encoded< MediaType::IMAGE> Encoded;
   };

}

#endif
