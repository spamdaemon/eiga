#ifndef _EIGA_DECODER_H
#define _EIGA_DECODER_H

#ifndef _EIGA_H
#include <eiga/eiga.h>
#endif

#ifndef _EIGA_MEDIATYPE_H
#include <eiga/MediaType.h>
#endif

#ifndef _EIGA_ENCODED_H
#include <eiga/Encoded.h>
#endif

#include <functional>

namespace eiga {

   /**
    * An encoder for input data.
    */
   template<MediaType MT>
   class Decoder
   {
         Decoder(const Decoder&) = delete;
         Decoder& operator=(const Decoder&) = delete;

         /** The input type */
      public:
         typedef typename CodecHelper< MT>::Decoded Output;

         /** The output type */
      public:
         typedef typename CodecHelper< MT>::Encoded Input;

         /** The output function */
      public:
         typedef ::std::function< void(const Output&)> OutputCB;

         /** Constructor */
      protected:
         Decoder() throws()
         {
         }

         /** Destructor */
      public:
         virtual ~Decoder() throws() =0;

         /**
          * Decode an input object.
          * @param input the data to be decoded
          * @param output an output callback that will be invoked for each output produced
          * @return the number of times the output callback was invoked
          */
      public:
         size_t decode(const Input& input, OutputCB& output) = 0;
   };

   template<MediaType MT> Decoder< MT>::~Decoder() throws()
   {
   }

}

#endif
