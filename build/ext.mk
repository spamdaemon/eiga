# this makefile fragment defines the external libraries
# for the libraries, examples, tests, and binaries
# 
# the following definitions must be output from this file:
# EXT_LIB_SEARCH_PATH : the search paths for libraries
# EXT_LINK_LIBS       : the libraries with which to link (in the proper link order)
# EXT_INC_SEARCH_PATH : the search paths for include files
EXT_LIB_SEARCH_PATH :=
EXT_LINK_LIBS       :=
EXT_INC_SEARCH_PATH :=

# this is an optional definition
# EXT_COMPILER_DEFINES : definitions passed to the compiler (without the -D option)
#EXT_CPP_OPTS := compiler options for c++ compilation
#EXT_C_OPTS := compiler options for c compilation
#EXT_FORTRAN_OPTS := compiler options for fortran compilation

EXT_COMPILER_DEFINES :=
EXT_CPP_OPTS := 
EXT_C_OPTS := 
EXT_FORTRAN_OPTS := 

# theora support

ENABLE_EIGA_OGG_THEORA := $(call DIR_EXISTS,"/usr/include/theora")
$(eval $(call UPDATE_COMMON,ENABLE_EIGA_OGG_THEORA,,,-ltheora -ltheoraenc -ltheoradec -logg))

#$(error "HERE", $(EXT_COMPILER_DEFINES), $(EXT_LINK_LIBS))

EXT_COMPILER_DEFINES += ENABLE_EIGA_OGG_THEORA=$(ENABLE_EIGA_OGG_THEORA)
ifeq ($(ENABLE_EIGA_OGG_THEORA),1)
#EXT_INC_SEARCH_PATH += /usr/include/theora /usr/include/ogg
EXT_LINK_LIBS	+= -ltheora -ltheoraenc -ltheoradec -logg
endif


# h264 support
ENABLE_EIGA_H264 := $(call DIR_EXISTS,"$(PROJECT_DIR)/externals/x264")

EXT_COMPILER_DEFINES += ENABLE_EIGA_H264=$(ENABLE_EIGA_H264)
ifeq ($(ENABLE_EIGA_H264),1)
EXT_INC_SEARCH_PATH += $(PROJECT_DIR)/externals/x264/include/
EXT_LIB_SEARCH_PATH += $(PROJECT_DIR)/externals/x264/lib
EXT_LINK_LIBS	+= -lx264
endif

# mp4v2 support (needed to write h264 in the mp4 container)
ENABLE_EIGA_MP4  :=  $(call DIR_EXISTS,"$(PROJECT_DIR)/externals/mp4v2")
EXT_COMPILER_DEFINES += ENABLE_EIGA_MP4=$(ENABLE_EIGA_MP4)
ifeq ($(ENABLE_EIGA_MP4),1)
EXT_INC_SEARCH_PATH += $(PROJECT_DIR)/externals/mp4v2/include/
EXT_LIB_SEARCH_PATH += $(PROJECT_DIR)/externals/mp4v2/lib64
EXT_LINK_LIBS   += -lmp4v2
endif

# gpac support
ENABLE_EIGA_GPAC := $(call DIR_EXISTS,"$(PROJECT_DIR)/externals/gpac")
EXT_COMPILER_DEFINES += ENABLE_EIGA_MP4=$(ENABLE_EIGA_GPAC)
ifeq ($(ENABLE_EIGA_GPAC),1)
EXT_LIB_SEARCH_PATH += $(PROJECT_DIR)/externals/gpac/lib
EXT_LINK_LIBS	+= -lgpac
endif

