#include <canopy/video/VideoSource.h>
#include <canopy/video/FrameGrabber.h>
#include <canopy/video/Format.h>

#include <canopy/time/Time.h>
#include <canopy/fs/FileSystem.h>
#include <canopy/mt/Thread.h>

#include <timber/media/YUVImage.h>
#include <timber/media/ImageBuffer.h>
#include <timber/media/IntensityImage.h>
#include <timber/media/ImageFactory.h>
#include <timber/media/format/Jpeg.h>

#include <eiga/codec/Theora.h>
#include <eiga/codec/H264.h>

#include <timber/media/image/ScaleOp.h>
#include <timber/Date.h>
#include <timber/logging.h>

#include <cstring>
#include <string>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include <cstdlib>

using namespace std;
using namespace canopy;
using namespace canopy::fs;
using namespace canopy::video;
using namespace canopy::time;
using namespace timber;
using namespace timber::logging;
using namespace timber::media;
using namespace timber::media::format;
using namespace timber::media::image;
using namespace eiga::codec;

static bool _useTheora = false;
static bool _useH264 = false;
static ULong clipLengthSec = 300; // 5min clips by default

static Log logger()
{
   return "VideoRecorder";
}

struct ::std::shared_ptr<ImageBuffer> crop(ImageBuffer& image, size_t x, size_t y, size_t width, size_t height)
{
   if (x + width > image.width() || y + height > image.height()) {
      return ::std::shared_ptr<ImageBuffer>();
   }

   ::std::unique_ptr< ImageBuffer::PixelColor[]> pixels(new ImageBuffer::PixelColor[width * height]);
   for (size_t i = 0; i < height; ++i) {
      for (size_t j = 0; j < width; ++j) {
         image.pixel(x + j, y + i, pixels[i * width + j]);
      }
   }
   return ImageBuffer::createImage(width, height, move(pixels));
}

struct MyRecorderCallback
{
      MyRecorderCallback(ostream* out, const char* filename, ULong nFrames, unique_ptr< ScaleOp> op,
            bool writeGrayImage, bool oneSecRec)
            : _stream(out), _root(filename == 0 ? "." : filename), _file(
                  (filename == 0 || oneSecRec) ? 0 : new ofstream(filename, ::std::ios::binary)), _remaining(nFrames), _gray(
                  writeGrayImage), _op(move(op)), _oneSecRec(oneSecRec), _firstTime(Time::now()), _lastTime(
                  Time::now()), _currentInterval(0), _factory(ImageFactory::getBuiltinFactory())
      {
         _nFrames = 0;
         _lastTime = Time(0);
      }

      ~MyRecorderCallback()throws()
      {
         _h264.reset(nullptr);
         _theora.reset(nullptr);
      }

      bool operator()(const FrameGrabber::FrameInfo& info)
      {
         size_t frameNo = info.frameNo;
         size_t x = info.x;
         size_t y = info.y;
         size_t w = info.w;
         size_t h = info.h;
         const ::canopy::video::Format& encoding = info.format;
         const char* buf = info.buf;
         size_t bufSize = info.bufSize;

         LogEntry(logger()).info() << "FrameNo: " << frameNo << ", x=" << x << ", y=" << y << ", w=" << w << ", h=" << h
               << ", encoding=" << encoding.string() << ",  bufsize=" << bufSize << doLog;

         int fps = 5;

         const Time now = Time::now();

         // format the current time and create the necessary directories
         if (_useTheora || _useH264) {
            if ((_currentInterval.timeElapsed().count() / (clipLengthSec * 1000000000LL))
                  != (now.timeElapsed().count() / (clipLengthSec * 1000000000LL))) { // 5min
               if (_useTheora) {
                  _theora.reset(nullptr);
               }
               if (_useH264) {
                  _h264.reset(nullptr);
               }
            }

            _currentInterval = now;
            if (_theora  == nullptr || _h264 == nullptr) {
               string curFile;

               const TimeOfDay tod = Date(now).timeOfDay();

               FileSystem fs;
               string curDir = "clips" + fs.directorySeparator() + now.formatDate();
               try {
                  if (!fs.exists(curDir)) {
                     fs.mkdirs(curDir);
                  }
                  ostringstream out;
                  out << curDir << fs.directorySeparator() << setfill('0') << setw(2) << tod.hour() << setfill('0')
                        << setw(2) << tod.minute() << setfill('0') << setw(2) << tod.second();
                  curFile = out.str();
               }
               catch (...) {
               }

               try {
                  string oldDir = "clips" + fs.directorySeparator()
                        + Time(now.time() - Time::Duration(3LL * 24 * 3600 * 1000000000LL)).formatDate();
                  if (fs.exists(oldDir)) {
                     fs.rmdir(oldDir, true);
                     LogEntry(logger()).info() << "Removed " << oldDir << doLog;
                  }
               }
               catch (const exception& ex) {
               }
               if (_useTheora && _theora == nullptr) {
                  Theora::EncodeParameters cfg;
                  cfg.width = w;
                  cfg.height = h;
                  cfg.fps = fps;
                  unique_ptr< ostream> videoStream(new ofstream((curFile + ".ogg").c_str(), ofstream::binary));
                  _theora = Theora::createEncoder(move(videoStream), cfg);
                  _firstTime = Time::now();
               }
               if (_useH264 && _h264 == nullptr) {
                  H264::EncodeParameters cfg;
                  cfg.width = w;
                  cfg.height = h;
                  cfg.fps = fps;
                  unique_ptr< ostream> videoStream(new ofstream((curFile + ".mp4").c_str(), ofstream::binary));
                  _h264 = H264::createEncoder(move(videoStream), cfg);
                  _firstTime = Time::now();
               }
            }
         }

         ::std::shared_ptr< ImageBuffer> image;
         if (encoding.encoding() == ::canopy::video::Format::JPEG) {
            istringstream in(string(buf, bufSize));
            image = ::timber::media::format::Jpeg::read(in, _factory);
         }
         else if (encoding.encoding() == ::canopy::video::Format::YUYV) {

            unique_ptr< char[]> yuv(new char[3 * w * h]);
            char* py = yuv.get();
            char* pu = py + w * h;
            char* pv = pu + w * h;

            for (UInt32 i = 0; i < h; ++i) {
               // going to w-1 avoids a guaranteed buffer overflow in case w is not even
               for (UInt32 j = 0; j < w - 1; j += 2) {
                  const char* start = buf + 2 * (i * w + j);
                  // first get the actual YUV values
                  int Y0 = (unsigned char) *(start);
                  int U = (unsigned char) *(start + 1);
                  int Y1 = (unsigned char) *(start + 2);
                  int V = (unsigned char) *(start + 3);

                  (*py++) = Y0;
                  (*pu++) = U;
                  (*pv++) = V;

                  (*py++) = Y1;
                  (*pu++) = U;
                  (*pv++) = V;
               }
            }

            ::std::shared_ptr< YUVImage> yuvImage = ::timber::media::YUVImage::createImage(w, h, move(yuv));
            image = yuvImage;

            if ((_theora.get() || _h264.get()) && image) {
               if (((fps * now.timeElapsed().count()) / 1000000000)
                     != ((fps * _lastTime.timeElapsed().count()) / 1000000000)) {
                  if (_theora.get()) {
                     _theora->write(*yuvImage);
                  }
                  if (_h264.get()) {
                     _h264->write(*yuvImage);
                  }
                  _lastTime = now;
                  ++_nFrames;
               }
            }
         }
         else if (encoding.encoding() == ::canopy::video::Format::YUV420) {
            unique_ptr< char[]> yuv(new char[3 * w * h]);
            char* py = yuv.get();
            char* pu = py + w * h;
            char* pv = pu + w * h;

            const char* yStart = buf;
            const char* uStart = yStart + (w * h);
            const char* vStart = uStart + (w * h) / 4;
            for (UInt32 i = 0; i < h; ++i) {
               for (UInt32 j = 0; j < w; ++j) {
                  (*py++) = (unsigned char) yStart[i * w + j];
                  (*pu++) = (unsigned char) uStart[(i / 2) * (w / 2) + (j / 2)];
                  (*pv++) = (unsigned char) vStart[(i / 2) * (w / 2) + (j / 2)];
               }
            }
            image = YUVImage::createImage(w, h, move(yuv));
         }
         else if (encoding.encoding() == ::canopy::video::Format::GREY) {
            unique_ptr< char[]> gray(new char[w * h]);
            ::std::memcpy(gray.get(), buf, w * h);
            image = ImageBuffer::createGrayImage(w, h, move(gray));
         }
         else if (encoding.encoding() == ::canopy::video::Format::RGB24) {
            unique_ptr< char[]> rgb(new char[3 * w * h]);
            ::std::memcpy(rgb.get(), buf, 3 * w * h);
            image = ImageBuffer::createRGBImage(w, h, move(rgb));
         }
         else {
            LogEntry(logger()).severe() << "Format not understood : " << encoding.string() << doLog;

            // log the video data to a file
            {
               LogEntry(logger()).info() << "Logging video frame to 'video.frame'" << doLog;
               ofstream videoLog("video.frame");
               videoLog.write(buf, bufSize);
            }

            _remaining = 0;
            return false;
         }

         if (_op.get() != 0) {
            image = _op->apply(image);
         }

         if (_gray) {
            image = IntensityImage::createImage(image);
         }

         if (_oneSecRec) {
            FileSystem fs;
            // get the current date and time of day
            TimeOfDay tod = Date(now).timeOfDay();

            {
               ostringstream out;
               out << _root << fs.directorySeparator() << now.formatDate() << fs.directorySeparator() << setfill('0')
                     << setw(2) << tod.hour() << fs.directorySeparator() << setfill('0') << setw(2) << tod.minute();
               try {
                  fs.mkdirs(out.str());
                  out << fs.directorySeparator() << setfill('0') << setw(2) << tod.second() << ".jpg";
                  if (fs.exists(out.str())) {
                     ::canopy::mt::Thread::suspend(500000000);
                     return true;
                  }
                  ofstream file(out.str().c_str());
                  ::timber::media::format::Jpeg::write(*image, file);
                  file.flush();

                  ::canopy::mt::Thread::suspend(500000000);
               }
               catch (const exception& ex) {
                  LogEntry(logger()).severe() << "Could not create directory " << out.str() << " : " << ex.what()
                        << doLog;
               }
            }

            try {
               ostringstream out;
               out << _root << fs.directorySeparator()
                     << Time(now.time() - Time::Duration(3LL * 24 * 3600 * 1000000000LL)).formatDate();
               if (fs.exists(out.str())) {
                  fs.rmdir(out.str(), true);
                  LogEntry(logger()).info() << "Removed " << out.str() << doLog;
               }
            }
            catch (const exception& ex) {
            }

         }
         else if (_file.get() != 0) {
            image->writeSunRasterImage(*_file);
         }
         if (_stream) {
            image->writeSunRasterImage(*_stream);
         }

         return _oneSecRec || (--_remaining) > 0;
      }

   private:
      ostream* _stream;
      string _root;
      unique_ptr< ofstream> _file;
      ULong _remaining;
      bool _gray;
      unique_ptr< ScaleOp> _op;
      bool _oneSecRec;
      Time _firstTime;
      Time _lastTime;
      Time _currentInterval;
      size_t _nFrames;
      unique_ptr< Theora> _theora;
      unique_ptr< H264> _h264;
      ::timber::SharedRef< ::timber::media::ImageFactory> _factory;
};

int main(int argc, char** argv)
{
   const char* source = 0;
   const char* saveAs = 0;

   ULong nFrames = 0xffffffffffffffffULL;
   bool halfsize = false;
   bool writeGrayImage = false;
   bool record = false;
   bool nostdout = false;
   ULong w=320;
   ULong h=240;
   for (int arg = 1; arg < argc; ++arg) {
      if (string(argv[arg]) == "-record") {
         record = true;
      }
      if (string(argv[arg]) == "-theora") {
#if ENABLE_TIMBER_OGG_THEORA==1
	_useTheora = true;
#else
	cerr << "OGG/Theora not available" << endl;
	exit(1);
#endif
      }
      else if (string(argv[arg]) == "-h264") {
#if ENABLE_TIMBER_H264==1
	_useH264 = true;
#else
	cerr << "H264 not available" << endl;
	exit(1);
#endif
      }
      else if (string(argv[arg]) == "-no-stdout") {
         nostdout = true;
      }
      else if (string(argv[arg]) == "-device") {
         if (++arg == argc) {
            cerr << "Missing device name" << endl;
            exit(1);
         }
         source = argv[arg];
      }
      else if (string(argv[arg]) == "-o") {
         if (++arg == argc) {
            cerr << "Missing sun-raster outfile name" << endl;
            exit(1);
         }
         saveAs = argv[arg];
      }
      else if (string(argv[arg]) == "-w") {
         if (++arg == argc) {
            cerr << "Missing width" << endl;
            exit(1);
         }
         if (sscanf(argv[arg], "%lu", &w) != 1) {
            cerr << "Could not parse " << argv[arg] << " as an unsigned integer" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-h") {
         if (++arg == argc) {
            cerr << "Missing height" << endl;
            exit(1);
         }
         if (sscanf(argv[arg], "%lu", &h) != 1) {
            cerr << "Could not parse " << argv[arg] << " as an unsigned integer" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-n") {
         if (++arg == argc) {
            cerr << "Missing number of frames" << endl;
            exit(1);
         }
         if (sscanf(argv[arg], "%lu", &nFrames) != 1) {
            cerr << "Could not parse " << argv[arg] << " as an unsigned integer" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-i") {
         if (++arg == argc) {
            cerr << "Missing clip length" << endl;
            exit(1);
         }
         if (sscanf(argv[arg], "%lu", &clipLengthSec) != 1) {
            cerr << "Could not parse " << argv[arg] << " as an unsigned integer" << endl;
            exit(1);
         }
      }
      else if (string(argv[arg]) == "-halfsize") {
         halfsize = true;
      }
      else if (string(argv[arg]) == "-gray") {
         writeGrayImage = true;
      }

   }
   unique_ptr< ScaleOp> scale;
   if (halfsize) {
      scale = ScaleOp::createHalfResolution();
   }
   unique_ptr< VideoSource> vid;
   try {
      vid = VideoSource::open(source);
      LogEntry(logger()).info() << "Successfully opened the video device" << doLog;
      FrameGrabber& fg = vid->frameGrabber();
      fg.setImageSize(w,h);
      LogEntry(logger()).info() << "Image size : " << fg.imageWidth() << "x" << fg.imageHeight() << doLog;
     ostream* xout = &cout;
      if (nostdout) {
         xout = 0;
      }
      MyRecorderCallback cb(xout, saveAs, nFrames, move(scale), writeGrayImage, record);
      FrameGrabber::FrameCallback xcb = [&] (const FrameGrabber::FrameInfo& info)->bool {return cb(info);};
      fg.captureFrames(xcb);
   }
   catch (const exception& e) {
      LogEntry(logger()).severe() << "Error opening the video device : " << e.what() << doLog;
      return 1;
   }
   return 0;
}
